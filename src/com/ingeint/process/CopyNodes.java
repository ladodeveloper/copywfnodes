package com.ingeint.process;

import java.util.List;
import java.util.logging.Level;

import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.DB;
import org.compiere.wf.MWFNode;
import org.compiere.wf.MWFNodeNext;
import org.compiere.wf.MWorkflow;

import com.ingeint.base.CustomProcess;

public class CopyNodes extends CustomProcess {

	protected Integer AD_Workflow_ID;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals(MWorkflow.COLUMNNAME_AD_Workflow_ID))
				AD_Workflow_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {

		MWorkflow wfFrom = new MWorkflow(getCtx(), AD_Workflow_ID, get_TrxName());
		MWorkflow wfTo = new MWorkflow(getCtx(), getRecord_ID(), get_TrxName());
		MWFNode[] nodes = getNodes(wfFrom);

		// Search the Parent nodes
		for (MWFNode node : nodes) {

			MWFNode newNode = new MWFNode(getCtx(), 0, null);
			MWFNode.copyValues(node, newNode);
			newNode.save();

			DB.executeUpdateEx("UPDATE AD_WF_Node set AD_Workflow_ID = ? " + "WHERE AD_WF_Node_ID = ? ",
					new Object[] { wfTo.get_ID(), newNode.get_ID() }, null);

			if (node.getNextNodeCount() > 0) {

				MWFNodeNext[] nodenexts = getNodesNext(node);

				for (MWFNodeNext ne : nodenexts) {

					MWFNodeNext newNext = new MWFNodeNext(getCtx(), 0, null);
					MWFNodeNext.copyValues(ne, newNext);
					newNext.save();
					
					DB.executeUpdateEx("UPDATE AD_WF_NodeNext set AD_WF_Node_ID = ? "
							+ "WHERE AD_WF_NodeNext_ID = ? ", new Object[] {newNode.get_ID(), newNext.get_ID()}, null);
				}
			}
			
			wfTo.setAD_WF_Node_ID(wfFrom.getAD_WF_Node_ID());
			wfTo.saveEx();
		}

		return "@Processed@";
	}

	public MWFNode[] getNodes(MWorkflow wf) {

		List<MWFNode> nodes = new Query(getCtx(), MWFNode.Table_Name, MWFNode.COLUMNNAME_AD_Workflow_ID + "= ?",get_TrxName())
				.setParameters(wf.get_ID())
				.setOrderBy(MWFNode.COLUMNNAME_Value).list();

		return nodes.toArray(new MWFNode[nodes.size()]);

	}

	public MWFNodeNext[] getNodesNext(MWFNode node) {

		List<MWFNodeNext> list = new Query(getCtx(), MWFNodeNext.Table_Name, "AD_WF_NodeNext.AD_WF_Node_ID=?",
				get_TrxName()).addJoinClause(
						" JOIN AD_WF_Node ON (AD_WF_Node.AD_WF_Node_ID=AD_WF_NodeNext.AD_WF_Next_ID AND AD_WF_Node.IsActive='Y')")
						.setParameters(new Object[] { node.get_ID() }).setOnlyActiveRecords(true)
						.setOrderBy(MWFNodeNext.COLUMNNAME_SeqNo).list();

		return list.toArray(new MWFNodeNext[list.size()]);
	}

}
